<?php

/**
 * Settings form.
 *
 * @return system_settings_form
 */
function nodetypetheme_form()
{
  $form = array();
  $themes = array(NODETYPETHEME_DEFAULT => '');
  $themesinfo = list_themes();
  $types = node_get_types('names');
  
  while($themeinfo = array_shift($themesinfo))
  {
  	$themes[$themeinfo->name] = $themeinfo->info['name'];
  }
  unset($themesinfo);
  asort($themes);
  $themes[NODETYPETHEME_DEFAULT] = t('System default');
  
  foreach($types as $type => $name)
  {
    $form['nodetypetheme_' . $type] = array(
      '#title' => $name,
      '#type' => 'select',
      '#default_value' => nodetypetheme_get_theme($type),
      '#options' => $themes,
    );
  }
  
  return system_settings_form($form);
}

